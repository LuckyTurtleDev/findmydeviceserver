# Web Readme

We bundle the following dependencies:

- https://unpkg.com/leaflet@1.9.4/dist/leaflet.js
- https://unpkg.com/leaflet@1.9.4/dist/leaflet.css
- https://unpkg.com/toastedjs@0.0.2/dist/toasted.min.js
- https://unpkg.com/toastedjs@0.0.2/dist/toasted.min.css
- https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.js
- https://cdnjs.cloudflare.com/ajax/libs/jsencrypt/2.3.1/jsencrypt.min.js

Note that `crypto-js` and `jsencrypt` can be removed once support for
the pre-0.4.0 crypto is dropped.

To update one of these dependencies, simply download them from the above URLs
with a new version and commit it.

