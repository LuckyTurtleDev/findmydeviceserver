module findmydeviceserver

go 1.17

require (
	github.com/google/flatbuffers v23.5.26+incompatible
	github.com/objectbox/objectbox-go v1.6.1
	golang.org/x/crypto v0.9.0
)

require github.com/objectbox/objectbox-generator v0.13.0 // indirect
